function pageload() {

    var path = window.location.pathname;

    if (path != '/') {
        var sessionid = path.match(/\/([^\/]+)/)[1] || null;
        document.getElementById('sessionid').value = sessionid;
        document.getElementById('helptext').classList.add('d-none');
    }
    document.getElementById('name').focus();
}

function showAdvancedOptions(button) {

    advancedOptions = document.getElementById('advanced').classList.remove('d-none');
    button.classList.add('d-none');
}

function setCustomScores(scores) {
    document.getElementById('scoreValues').value = scores;
}

function join() {

    window.session.name = window.session.name || document.getElementById('name').value;
    window.session.scoreValues = document.getElementById('scoreValues').value;
    window.session.sessionid = window.session.sessionid || document.getElementById('sessionid').value;
    window.session.userid = window.session.userid || parseInt(localStorage.getItem('userid'))

    document.getElementById('who-are-you').classList.add('d-none');
    document.getElementById('session').classList.remove('d-none');

    postData('/api/join', window.session)
        .then((data) => {
            updateSession(data);
            history.replaceState(0, '', window.session.sessionid);
            document.getElementById('session-link').setAttribute('href', window.location);
            localStorage.setItem("userid", data.userid);
            autoPoll();
        });

}


function vote(score) {

    document.getElementById(window.session.userid + '-score').innerText = score;

    window.session.score = score;

    postData('/api/vote', window.session)
        .then((data) => {
            updateSession(data)
        });

}

function reset() {

    scores = document.getElementsByClassName('score')

    for (var i = scores.length - 1; i >= 0; i--) {
        scores[i].innerText = null
    }

    postData('/api/reset', window.session)
        .then((data) => {
            updateSession(data)
        });

}

function autoPoll() {

    getData('/api/session/' + window.session.sessionid)
        .then((data) => {
            updateSession(data)
            setTimeout(autoPoll, 3000);
        });

}

function leave() {

    postData('/api/leave', window.session)
        .then((data) => {
            updateSession(data)
        });
}

function updateSession(session) {

    var scoreButtons = document.getElementById('score-buttons');

    if (scoreButtons.innerText == "") {

        var scoreValues = session.scoreValues.split(',');

        for (var i = scoreValues.length - 1; i >= 0; i--) {

            var trimmedScore = scoreValues[i].trim();

            if( trimmedScore ){
                var buttonNode = "<button type=\"button\" class=\"btn btn-primary\" value=\"" + trimmedScore + "\" onclick=\"vote(this.value)\">" + scoreValues[i].trim() + "</button> "
                document.getElementById('score-buttons').insertAdjacentHTML("afterbegin", buttonNode)
            }

        }

    }

    for (var i = session.participants.length - 1; i >= 0; i--) {

        var id = session.participants[i].id;
        var name = session.participants[i].userdetails.name;
        var score = session.participants[i].userdetails.score || "";

        var scoreElement = document.getElementById(id + '-score');
        var nameElement = document.getElementById(id + '-name');

        if (!scoreElement) {
            var participantNode = "<tr><td id='" + id + "-name'></td><td class='score' id='" + id + "-score'></td></tr>"
            document.getElementById('participants').insertAdjacentHTML("beforeend", participantNode)
            document.getElementById(id + '-name').innerText = name;
            document.getElementById(id + '-score').innerText = score;
        } else {
            scoreElement.innerText = score;
            nameElement.innerText = name;
        }
    }

    if (session.userid) {
        window.session.userid = session.userid;
    }

    window.session.sessionid = session.sessionid;
    document.getElementById('session-header').innerText = "Session: " + session.sessionid;

    console.log(JSON.stringify(window.session))

}

//Don't understand any of this. Stole it from https://googlechrome.github.io/samples/fetch-api/fetch-post.html

async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return await response.json();
}

async function getData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url);
    return await response.json(); // parses JSON response into native JavaScript objects
}