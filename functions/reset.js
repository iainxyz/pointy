if (process.env.NETLIFY_DEV) {
    require('dotenv').config()
}

const { MongoClient } = require('mongodb');

const DB_URL = process.env.DB_URL;
const DB_NAME = 'pointy';
const DB_COLLECTION = 'sessions';

function errorResponse(callback, responseCode, err) {
    console.error(err);

    callback(null, {
        statusCode: responseCode,
        body: JSON.stringify({ error: err })
    })
};


exports.handler = function(event, context, callback) {

    console.log("-----\nevent : " + JSON.stringify(event) + "\n-----\n");

    if (event.hasOwnProperty('body') && event.body && event.body.toString().length > 0) {
        var body = JSON.parse(event.body);
        console.log(body);
        console.log("Request Body : " + JSON.stringify(body));
    } else {
        console.error("No Request Body");
    }

    var sessionid = body.sessionid.toString();

    MongoClient.connect(`${DB_URL}/${DB_NAME}`, (err, connection) => {
        if (err) return errorResponse(callback, 500, err);

        const db = connection.db(DB_NAME);
        const collection = db.collection(DB_COLLECTION);

        const searchData = { sessionid: sessionid };

        const insertData = {
            $set: { "lastModifiedDate": new Date() },
            $set: { "data.participants.$[].userdetails.score": null }
        }

        collection.updateOne(searchData, insertData, { upsert: true }, (err, result) => {
            if (err) return errorResponse(callback, 500, err);

            // get updated session back out and return in response.
            collection.findOne({ sessionid: sessionid }, (err, result) => {
                if (err) return errorResponse(callback, 500, err);

                result.data.sessionid = sessionid;

                var responseHeaders = {
                    "Content-Type": "application/json"
                };
                var responseObject = {
                    statusCode: 200,
                    headers: responseHeaders,
                    body: JSON.stringify(result.data)
                };

                connection.close();

                callback(null, responseObject);

            });

        });

    });

};