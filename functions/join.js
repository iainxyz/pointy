if (process.env.NETLIFY_DEV) {
    require('dotenv').config()
}

const { MongoClient } = require('mongodb');

const DEFAULT_SCORE_VALUES = "?,1,2,3,5,8,∞";

const DB_URL = process.env.DB_URL;
const DB_NAME = 'pointy';
const DB_COLLECTION = 'sessions';

function errorResponse(callback, responseCode, err) {
    console.error(err);

    callback(null, {
        statusCode: responseCode,
        body: JSON.stringify({ error: err })
    })
}

exports.handler = function(event, context, callback) {

    console.log("-----\nevent : " + JSON.stringify(event) + "\n-----\n");

    if (event.hasOwnProperty('body') && event.body && event.body.toString().length > 0) {
        var body = JSON.parse(event.body);
        console.log(body);
    } else {
        console.error("No Request Body");
        return errorResponse(callback, 400, "Request body missing or malformed");
    }

    //create a random user and session id if not in body
    var sessionid = body.sessionid.toString() || Math.floor((Math.random() * 99999) + 1).toString();
    var userid = body.userid || Math.floor((Math.random() * 99999999) + 1);
    var name = body.name;

    //TODO : Clean up scoreValues on insert. Remove trailing whitespace and blanks.
    var scoreValues = body.scoreValues || DEFAULT_SCORE_VALUES;

    MongoClient.connect(`${DB_URL}/${DB_NAME}`, (err, connection) => {
        if (err) return errorResponse(callback, 500, err);

        const db = connection.db(DB_NAME);
        const collection = db.collection(DB_COLLECTION);

        const searchData = { sessionid: sessionid }

        const initData = {
            $pull: {
                "data.participants": { id: userid }
            },
            $setOnInsert: {
                "data.scoreValues": scoreValues
            }
        };

        const updateData = {
            $set: {
                "lastModifiedDate": new Date()
            },
            $push: {
                "data.participants": { id: userid, userdetails: { name: name, score: null } }
            }
        };

        collection.updateOne(searchData, initData, { upsert: true }, (err, result) => {
                if (err) return errorResponse(callback, 500, err);

            collection.updateOne(searchData, updateData, { upsert: true }, (err, result) => {
                if (err) return errorResponse(callback, 500, err);

                // get updated session back out and return in response.
                collection.findOne(searchData, (err, result) => {
                    if (err) return errorResponse(callback, 500, err);

                    result.data.name = name;
                    result.data.userid = userid;
                    result.data.sessionid = sessionid;

                    var responseHeaders = {
                        "Content-Type": "application/json"
                    };
                    var responseObject = {
                        statusCode: 200,
                        headers: responseHeaders,
                        body: JSON.stringify(result.data)
                    };

                    connection.close();

                    callback(null, responseObject);


                });

            });

        });


    });



};