if (process.env.NETLIFY_DEV) {
    require('dotenv').config()
}

const { MongoClient } = require('mongodb');

const DB_URL = process.env.DB_URL;
const DB_NAME = 'pointy';
const DB_COLLECTION = 'sessions';

function errorResponse(callback, responseCode, err) {
    console.error(err);

    callback(null, {
        statusCode: responseCode,
        body: JSON.stringify({ error: err })
    })
}

exports.handler = function (event, context, callback) {

	console.log("-----\nevent : " + JSON.stringify(event) + "\n-----\n");

	var sessionid = event.path.match(/\/session\/([^\/]+)/)[1]
    //event.path == "/.netlify/functions/session/:sessionid/"
	//event.path == "/api/session/:sessionid/"

    if (event.httpMethod = "GET" && sessionid ) {
        console.log("GET SESSION: " + sessionid);
    } else {
        console.error("No session ID");
        return errorResponse(callback, 400, "bad request");
    }

    MongoClient.connect(`${DB_URL}/${DB_NAME}`, (err, connection) => {
        if (err) return errorResponse(callback, 500, err);

        const db = connection.db(DB_NAME);
        const collection = db.collection(DB_COLLECTION);


            collection.findOne({ sessionid: sessionid }, (err, result) => {
                if (err) return errorResponse(callback, 500, err);

                if (!result) return errorResponse(callback, 404, "Not Found")

				console.log(result);

                result.data.sessionid = sessionid;

                var responseHeaders = {
                    "Content-Type": "application/json"
                };
                var responseObject = {
                    statusCode: 200,
                    headers: responseHeaders,
                    body: JSON.stringify(result.data)
                };

                connection.close();

                callback(null, responseObject);

            });

    });
};